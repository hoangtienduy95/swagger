import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AccountService {

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line: variable-name
  getAccountList(currentPage= 1, pageSize= 10, sorted_field= '', sort_type= '') {
    // tslint:disable-next-line: max-line-length
    return this.http.get('http://localhost:8090/account_api/account?page=' + currentPage + '&limit=' + pageSize + '&sorted_field=' + sorted_field + '&sort_type=' + sort_type);
  }

  getAccountDetail(id: string) {
    return this.http.get('http://localhost:8090/account_api/account/' + id);
  }

  // tslint:disable-next-line: variable-name
  postAccount(posted_data: any) {
    return this.http.post('http://localhost:8090/account_api/account', posted_data);
  }

  // tslint:disable-next-line:variable-name
  putAccount(id, putted_data) {
    return this.http.put('http://localhost:8090/account_api/account/' + id, putted_data);
  }

  deleteAccount(id) {
    return this.http.delete('http://localhost:8090/account_api/account/' + id);
  }
}
