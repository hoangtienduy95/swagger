import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoginService{

  constructor(private http: HttpClient){ }

  Login(data_login){
    return this.http.post('http://localhost:8090/login_api/login', data_login);
  }
}
