import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthenticService {
  cookieValue: any;
  constructor(private cookieService: CookieService){}

  setRole(role) {
    this.cookieService.set( 'role', role );
    const cookieValue = this.cookieService.get('role');
  }
}
