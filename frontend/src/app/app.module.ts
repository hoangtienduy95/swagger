import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NotifierModule } from 'angular-notifier';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import 'hammerjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AccountFormComponent } from './form/account-form.component'
import { customNotifierOptions } from './notification/customNotifier';
import { AccountComponent } from './account/account.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import {AuthenticService} from './service/authentic.service';
import {AccountService} from './service/account.service';
import { LoginService } from './service/login.service';
import { CookieService } from 'ngx-cookie-service';
import { HomeComponent } from './home/home.component';

import { HomeModule } from './home/home.module';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccountComponent,
    AccountDetailComponent,
    AccountFormComponent,
    ConfirmDialogComponent,
    // HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    NgbModule,
    NotifierModule.withConfig(customNotifierOptions),
    HomeModule,
    RouterModule
  ],
  providers: [
    AccountService,
    AuthenticService,
    LoginService,
    CookieService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmDialogComponent]
})
export class AppModule { }
