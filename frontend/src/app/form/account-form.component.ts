import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup } from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';

import { DynamimcController } from '../shared/dynamic-form/dynamic-controller';
import { AccountModel } from '../model/account-model';
import { AccountService } from '../service/account.service';
import { Notification } from '../notification/customNotifier';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-account-form',
  templateUrl: '../shared/dynamic-form/dynamic-form.component.html',
  styleUrls: ['../shared/dynamic-form/dynamic-form.component.scss'],
  providers: [ DynamimcController, AccountModel, AccountService, Notification ]
})

export class AccountFormComponent implements OnInit {
	@Input() form: FormGroup;
	public title_form;
	public model: any[];
	public form_invalid = false;
  public row_form: any;
  public existed_email: any;
  public ngOn1: any;
  public function_button: any;
  public id: any;
  public data_Form = {};
  public min_age_invalid = false;
  public min_balance_invalid = false;
  public required_email_invalid: boolean;
  public email_invalid = false;
  public role: any;

  constructor(
    private location: Location,
    private controller: DynamimcController,
    private accountModel: AccountModel,
    private accountService: AccountService,
    private notification: Notification,
    private route: ActivatedRoute,
    private router: Router ,
    private cookieService: CookieService,
  ) {
    this.model = accountModel.getAccount();
    this.title_form = accountModel.title;
  }

  public removeEmptyOrNull = (obj) => {
    Object.keys(obj).forEach(k =>
      (obj[k] && typeof obj[k] === 'object') && this.removeEmptyOrNull(obj[k]) ||
      (!obj[k] && obj[k] !== undefined) && delete obj[k]
    );
    return obj;
  };

  ngOnInit() {
    this.role = this.cookieService.get('role');
    if (this.role != 1) {
      this.router.navigate(['/']);
    }
    this.row_form = 2;
    this.ngOn1 = this.route.paramMap.subscribe((params: ParamMap) => {
    this.id = params.get('id');
      if (this.id) {
        this.form = this.controller.toFormGroup(this.model, this.data_Form);
        this.function_button = 'Update';
        this.accountService.getAccountDetail(this.id).subscribe((data: any) => {
          this.data_Form = data;
          this.form = this.controller.toFormGroup(this.model, this.data_Form);
        });
      }
      else {
        this.function_button = 'Create';
        this.form = this.controller.toFormGroup(this.model, this.data_Form);
      }
    });
  }

  onSubmit(event) {
    if (this.form.valid) {
      if (this.id) {
        const data_form = this.form.value;
        const puted_data = this.removeEmptyOrNull(data_form);
        this.accountService.putAccount(this.id, puted_data).subscribe((data: any) => {
          if (data.Message === 'Email is existed') {
            this.existed_email = true;
          }
          if (data['age'] !== undefined) {
            if (data.age[0] === 'Must be greater than or equal to 1.') {
              this.min_age_invalid = true;
            }
          }
          if (data['balance'] !== undefined) {
            if (data.balance[0] === 'Must be greater than or equal to 1.') {
              this.min_balance_invalid = true;
            }
          }
          this.required_email_invalid = false;
          if (data['email'] !== undefined) {
            if (data.email[0] === 'Missing data for required field.') {
              this.required_email_invalid = true;
            }
            if (data.email[0] === 'Not a valid email address.') {
              this.email_invalid = true;
            }
          }
          if (data.Message === 'Account update successfully') {
            this.notification.showNotification('success', 'Cập nhật thành công');
            this.form.reset();
            this.location.back();
          }
        });
        return ;
      }
      const data_form = this.form.value;
      const posted_data = this.removeEmptyOrNull(data_form);
      this.accountService.postAccount(posted_data).subscribe((data: any) => {
        console.log(data);
        if (data.Message === 'Email is existed') {
          this.existed_email = true;
        }
        if (data['age'] !== undefined) {
          if (data.age[0] === 'Must be greater than or equal to 1.') {
            this.min_age_invalid = true;
          }
        }
        if (data['balance'] !== undefined) {
          if (data.balance[0] === 'Must be greater than or equal to 1.') {
            this.min_balance_invalid = true;
          }
        }
        this.required_email_invalid = false;
        if (data['email'] !== undefined) {
          if (data.email[0] === 'Missing data for required field.') {
            this.required_email_invalid = true;
          }
          if (data.email[0] === 'Not a valid email address.') {
            this.email_invalid = true;
          }
        }
        if (data.Message === 'Create is success'){
          this.notification.showNotification('success', 'Thêm mới thành công');
          this.form.reset();
          this.location.back();
        }
      });
    }
    else {
      this.required_email_invalid = false;
      this.form_invalid = true;
    }
  }

  goBack(): void {
    this.location.back();
  }
}
