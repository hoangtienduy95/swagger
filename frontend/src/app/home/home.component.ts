import { Component, OnInit, NgModule } from '@angular/core';
import {Router} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  constructor(
    private router: Router ,
    private cookieService: CookieService,
  ) { }

  ngOnInit() {
    const role = this.cookieService.get('role');
    if (!role) {
      this.router.navigate(['/login']);
    }
  }

  logout() {
    this.cookieService.delete('role');
    this.router.navigate(['/login']);
  }

}
