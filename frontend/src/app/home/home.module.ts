import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from "./home.component";
import { RouterModule } from "@angular/router";
import {MatLineModule} from "@angular/material/core";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {HomeRoutingModule} from "./home-routing-module";



@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatLineModule,
    MatSidenavModule,
    MatListModule,
    HomeRoutingModule
  ],
   exports: [ HomeComponent ]
})
export class HomeModule { }
