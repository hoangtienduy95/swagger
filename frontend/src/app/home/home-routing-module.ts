import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from "../account/account.component";
import { AccountFormComponent } from "../form/account-form.component";
import {HomeComponent} from "./home.component";
import {AccountDetailComponent} from "../account-detail/account-detail.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'account',
        children: [
          { path: '', component: AccountComponent },
          { path: 'create', component: AccountFormComponent },
          { path: 'edit/:id', component: AccountFormComponent },
          { path: ':id', component: AccountDetailComponent }
        ],
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule { }
