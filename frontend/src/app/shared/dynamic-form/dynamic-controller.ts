import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { BaseForm } from '../base-form/base-form';

@Injectable()
export class DynamimcController {
	constructor(){}
	toFormGroup(fields: BaseForm<any>[], data) {
		let group = {};
		if(Object.entries(data).length > 0) fields.forEach(field => {
      let validator = [];
      if (field.required) {
        validator.push(Validators.required);
      }
      (field.min_length);
      {
        validator.push(Validators.minLength(field.min_length));
      }
      if (field.max_length) {
        validator.push(Validators.maxLength(field.max_length));
      }
      if (field.min) {
        validator.push(Validators.min(field.min));
      }
      if (field.max) {
        validator.push(Validators.max(field.max));
      }
      if (field.type === 'email') {
        validator.push(Validators.email);
      }
      for (let [key, value] of Object.entries(data[0])) {
        if (field.key === key) {
          group[field.key] = new FormControl(value, validator);
        }
      }
    });
    else {
			fields.forEach(field => {
				let validator = [];
				if (field.required) validator.push(Validators.required);
        if (field.min_length) {
					validator.push(Validators.minLength(field.min_length));
				}
				if (field.max_length){
					validator.push(Validators.maxLength(field.max_length));
				}
				if (field.min) {
					validator.push(Validators.min(field.min));
				}
				if (field.max) {
					validator.push(Validators.max(field.max));
				}
				if (field.type === 'email') {
          validator.push(Validators.email);
        }
				if (field.type === 'number') {
          field.value = 1;
        }
				group[field.key] = 	new FormControl(field.value, validator);
			});
		}
		return new FormGroup(group);
	}
}
