import { BaseForm } from './base-form'

export class TextAreaBase extends BaseForm<string>{
	controlType = 'textarea';
	type: string;

	constructor(options: {} = {}) {
		super(options);
    	this.type = options['type'] || '';
	}
}