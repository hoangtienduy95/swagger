import { BaseForm } from './base-form';

export class TextBoxBase extends BaseForm<string>{
	controlType = 'textbox';
  	type: string;

  	constructor( options: {} = {} ){
  		super(options);
    	this.type = options['type'] || '';
  	}
}