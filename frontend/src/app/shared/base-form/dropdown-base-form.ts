import { BaseForm } from './base-form';

export class DropDownBase  extends BaseForm<string>{
	controlType = 'dropdown';
  	options: {key: string, value: string}[] = [];
  	
  	constructor(options: {} = {}) {
	    super(options);
	    this.options = options['options'] || [];
  	}
}