export class BaseForm<T> {
	value: T;
	key: string;
	label: string;
	type: string;
	required: boolean;
	order: number;
	controlType: string;
	min: number;
  max: number;
  min_length: number;
  max_length: number;
  pattern: string;

	constructor(
		options: {
			value?: T;
			key?: string,
      type?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string
      min?: number
      max?: number
      min_length?: number
      max_length?: number
      pattern?: string
		} = {}
	){
		this.value = options.value;
	    this.key = options.key || '';
	    this.label = options.label || '';
	    this.type = options.type || '';
	    this.required = !!options.required;
	    this.order = options.order === undefined ? 1 : options.order;
	    this.controlType = options.controlType || '';
	    this.min = options.min || 0;
	    this.max = options.max || 0;
	    this.min_length = options.min_length || 0;
	    this.max_length = options.max_length || 0;
	    this.pattern = options.pattern || '';
	}
}
