import { Injectable } from '@angular/core';

import { BaseForm }     from '../shared/base-form/base-form';
import { DropDownBase } from '../shared/base-form/dropdown-base-form';
import { TextBoxBase }  from '../shared/base-form/textbox-base-form';

@Injectable()
export class AccountModel{
  title = "Tài khoản";
  getAccount(){
    let account: BaseForm<any>[] = [
      new TextBoxBase({
        key: "firstname",
        label: "Tên",
        value: '',
        require: true,
        order: 1,
        type: "text"
      }),

      new TextBoxBase({
        key: "lastname",
        label: "Họ",
        value: '',
        require: true,
        order: 2,
        type: "text"
      }),

      new TextBoxBase({
        key: "balance",
        label: "Số dư",
        value: '',
        min: 1,
        order: 3,
        type: "number"
      }),

      new TextBoxBase({
        key: "age",
        label: "Tuổi",
        value: '',
        min: 1,
        max: 100,
        order: 4,
        type: "number"
      }),

      new DropDownBase({
        key: "gender",
        label: "Giới tính",
        value: 'M',
        order: 5,
        options: [
		          	{key: 'M',  value: 'Nam'},
		          	{key: 'F',  value: 'Nữ'}
		        ],
      }),

      new TextBoxBase({
        key: "address",
        label: "Địa chỉ",
        value: '',
        pattern: "[a-zA-Z àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ.:?!()-=+@']*",
        order: 6,
        type: "text"
      }),

      new TextBoxBase({
        key: "employer",
        label: "Nghề nghiệp",
        value: '',
        pattern: "[a-zA-Z àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ.:?!()-=+@']*",
        order: 7,
        type: "text"
      }),

      new TextBoxBase({
        key: "email",
        label: "E-mail",
        value: '',
        require: true,
        order: 8,
        type: "email"
      }),

      new TextBoxBase({
        key: "city",
        label: "Thành phố",
        value: '',
        order: 9,
        type: "text"
      }),

      new TextBoxBase({
        key: "state",
        label: "Bang",
        value: '',
        order: 10,
        type: "text"
      }),
    ]
    return account.sort((a, b) => a.order - b.order);
  }
}
