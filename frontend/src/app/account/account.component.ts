import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import {Sort} from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { AccountService } from '../service/account.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { Notification } from '../notification/customNotifier';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-account',
  templateUrl: '../shared/base-table/base-table.component.html',
  styleUrls: ['../shared/base-table/base-table.component.scss'],
  providers: [ AccountService, Notification ]
})
export class AccountComponent implements OnInit {
  ngOn1: any;
  public displayedColumns: string[] = [
    'firstname', 'lastname', 'email', 'balance', 'gender',
    'age', 'employer', 'address', 'city', 'state', 'tool'
  ];
  public columns = {
  firstname: 'First name', lastname: 'Last name', email: 'Email', balance: 'Balance', gender: 'Gender',
  age: 'Age', employer: 'Employer', address: 'Address', city: 'City', state: 'State', tool: 'Tool'
  };
  public dataSource: [];
  public record_counted: any;
  public confirm_delete: any;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  currentPage: any;
	public pageSize: any;
	role: any;
  constructor(
    private accountService: AccountService,
    private notification: Notification,
    private modalService: NgbModal,
    private cookieService: CookieService

  ) { }

  ngOnInit() {
    this.role = this.cookieService.get('role');
    this.currentPage = this.paginator.pageIndex;
    this.pageSize = this.paginator.pageSize;
    this.ngOn1 = this.accountService.getAccountList(1, 10, '', '').subscribe((data: any) => {
      this.dataSource = data.results;
      this.record_counted = data.total;
    });
  }

  ngOnDestroy = () => {
    this.ngOn1.unsubscribe();
  };

  handlePage(event) {
    this.currentPage = this.paginator.pageIndex;
    this.pageSize = this.paginator.pageSize;
    this.accountService.getAccountList(this.currentPage + 1, this.pageSize, '', '').subscribe((data: any) => {
      this.dataSource = data.results;
      this.record_counted = data.total;
    });
  }

  sortData(sort: Sort) {
    this.currentPage = this.paginator.pageIndex;
    this.pageSize = this.paginator.pageSize;
    const sorted_field = sort.active;
    const sort_type = sort.direction;
    this.accountService.getAccountList(this.currentPage + 1, this.pageSize, sorted_field, sort_type).subscribe((data: any) => {
      this.dataSource = data.results;
      this.record_counted = data.total;
    });
  }

  deleteAccount(id) {
    this.currentPage = this.paginator.pageIndex + 1;
    this.pageSize = this.paginator.pageSize;
    this.confirm_delete = this.modalService.open(ConfirmDialogComponent, {});
    this.confirm_delete.result.then((data) => {
      if (data) {
        this.accountService.deleteAccount(id).subscribe((data: any) => {
          if (data.Message === 'Account deleted successfully') {
            this.notification.showNotification('success', 'Xóa thành công');
            this.accountService.getAccountList(this.currentPage, this.pageSize,  '', '').subscribe((data: any) => {
              this.dataSource = data.results;
              this.record_counted = data.total;
            });
          }
        });
      }
    }, (reason) => {

    });
  }

}
