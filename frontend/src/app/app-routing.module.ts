import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './account/account.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { AccountFormComponent } from './form/account-form.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    // children: [
    //   {
    //     path: 'account',
    //     component: AccountComponent,
    //   }
    // ]
  },
  // {
  //   path: 'account',
  //   children: [
  //     { path: '', component: AccountComponent },
  //     { path: 'create', component: AccountFormComponent },
  //     { path: 'edit/:id', component: AccountFormComponent }
  //   ],
  // },
  // { path: 'account/:id', component: AccountDetailComponent },
  { path: 'login', pathMatch: 'full', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
