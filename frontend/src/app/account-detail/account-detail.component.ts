import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute, ParamMap } from "@angular/router";

import { AccountService } from '../service/account.service';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.scss'],
})
export class AccountDetailComponent implements OnInit {
  data: any;
  public ngOn1: any;
  constructor(
    private accountService: AccountService,
    private location: Location,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.ngOn1 = this.route.paramMap.subscribe((params: ParamMap) => {
      const id = params.get('id');
      this.accountService.getAccountDetail(id).subscribe((data: any) => {
        this.data = data[0];
      });
    });
  }
  ngOnDestroy(){
      this.ngOn1.unsubscribe();
  }
  Back(){
    this.location.back();
  }

}
