import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../service/login.service';
import { AuthenticService } from '../service/authentic.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public form_invalid = false;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private authenticService: AuthenticService,
    private router: Router
  ) { }

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });
  Invalid: boolean;
  onSubmit(event){

    if (this.loginForm.valid){
      let data_login = this.loginForm.value;
      this.loginService.Login(data_login).subscribe((data: any) => {
        if (data['required_error'] != null) {
          this.form_invalid = true;
        }
        else {
          if (data === 'Invalid username or password'){
            this.Invalid = true;
          }
          if (data['Message'] === 'Successful login'){
            const role = data['role'];
            this.authenticService.setRole(role);
            this.router.navigate(['/']);
          }
        }
      });

    }
    else {
      this.form_invalid = true;
    }
  }
}
