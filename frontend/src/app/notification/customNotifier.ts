import { Injectable } from '@angular/core';
import {NotifierOptions, NotifierService} from 'angular-notifier';

@Injectable()
export class Notification{
  private readonly notifier: NotifierService;

  constructor( notifierService: NotifierService ){
   this.notifier = notifierService;
   }

  showNotification(type, message): void {
		this.notifier.notify(type, message);
	}
}
export const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'middle',
			distance: 12
		},
		vertical: {
			position: 'top',
			distance: 12,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 1000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};
