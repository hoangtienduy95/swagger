import bcrypt
from marshmallow import Schema, fields, validate, ValidationError
from flask import jsonify
from flask_restplus import Resource, Namespace
import requests

from config import API_URL

login_api = Namespace('login_api')


class LoginSchema(Schema):
    username = fields.Str(required=True, validate=validate.Length(min=1))
    password = fields.Str(required=True)


@login_api.route('/login')
class Login(Resource):
    @login_api.doc('login')
    def post(self):
        login_data = LoginSchema().load(login_api.payload)
        req = "{}/login_api/login".format(API_URL)
        response = requests.post(req, json=login_data)
        return jsonify(response.json())
