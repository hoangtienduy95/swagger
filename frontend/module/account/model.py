from flask import jsonify
from flask_restplus import Resource, Namespace
from marshmallow import Schema, fields, validate, ValidationError
import requests

from config import API_URL

account_api = Namespace('account_api')


class AccountSchema(Schema):
    balance = fields.Int(validate=validate.Range(min=1))
    firstname = fields.Str(missing="")
    lastname = fields.Str(missing="")
    age = fields.Int(validate=validate.Range(min=1))
    gender = fields.Str()
    address = fields.Str(missing="")
    employer = fields.Str(missing="")
    email = fields.Email(required=True)
    city = fields.Str(missing="")
    state = fields.Str(missing="")


class AccountListSchema(Schema):
    page = fields.Int()
    limit = fields.Int()
    sorted_field = fields.Str()
    sort_type = fields.Str()


parser = account_api.parser()
parser.add_argument('page', location='args')
parser.add_argument('limit', location='args')
parser.add_argument('sorted_field', location='args')
parser.add_argument('sort_type', location='args')
@account_api.route('/account')
class AccountList(Resource):
    @account_api.doc('get_list')
    def get(self):
        page = 1
        limit = 10
        sorted_field = ""
        sort_type = ""
        args = parser.parse_args()

        temp_data = {k: v for k, v in args.items() if v is not None}

        AccountListSchema().load(temp_data)

        if args['page']:
            page = int(args['page'])
        if args['limit']:
            limit = int(args['limit'])
        if args['sorted_field'] is not None:
            sorted_field = args['sorted_field']
        if args['sort_type'] is not None:
            sort_type = args['sort_type']
        req = "{}/account_api/account?page={}&limit={}&sorted_field={}&sort_type={}".format(API_URL, page, limit, sorted_field, sort_type)
        response = requests.get(req)
        if response.ok:
            return response.json()
        else:
            code = response.status_code
            response = response.json()
            return jsonify(response), code

    @account_api.doc('create_account')
    def post(self):
        account_data = account_api.payload
        req = "{}/account_api/account".format(API_URL)
        response = requests.post(req, json=account_data)
        if response.ok:
            return response.json()
        else:
            code = response.status_code
            response = response.json()
            return jsonify(response), code


@account_api.route('/account/<int:id>')
class Account(Resource):
    @account_api.doc('get_account')
    def get(self, id):
        req = "{}/account_api/account/{}".format(API_URL, id)
        response = requests.get(req)
        if response.ok:
            return jsonify(response.json())
        else:
            code = response.status_code
            response = response.json()
            return jsonify(response), code


    @account_api.doc('update_account')
    def put(self, id):

        account_data = account_api.payload
        req = "{}/account_api/account/{}".format(API_URL, id)
        response = requests.put(req, json=account_data)
        if response.ok:
            return jsonify(response.json())
        else:
            code = response.status_code
            response = response.json()
            return jsonify(response), code


    @account_api.doc('delete_account')
    def delete(self, id):
        req = "{}/account_api/account/{}".format(API_URL, id)
        response = requests.delete(req)
        if response.ok:
            return jsonify(response.json())
        else:
            code = response.status_code
            response = response.json()
            return jsonify(response), code
        return jsonify({"Message": "Account deleted successfully"})
