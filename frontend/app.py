# -*- coding: utf-8 -*-
from flask import Flask, render_template, jsonify
from flask_cors import CORS
from flask_restplus import Api
from werkzeug.exceptions import default_exceptions

import config
# Import API module
from module.account.model import account_api
from module.login.model import login_api


def create_app():
    """Creates the app."""
    # Initialize the app
    app = Flask(__name__)
    app.config.from_object(config)

    CORS(app)
    return app


def create_api(app):
    """ Create the api """
    api = Api(
        title='Api for bank page',
        version='1.0',
        description='A description'
    )
    api.add_namespace(account_api, path='/account_api')
    api.add_namespace(login_api, path='/login_api')
    api.init_app(app)


def configure_error_handlers(app):
  """
  Handle error process
  :param app: flask app
  :return: not return
  """

  @app.errorhandler(400)
  def bad_request(error):
    app.logger.error(error, exc_info=error)
    if type(error.description) == dict:
      return jsonify(error=error.description), 400
    else:
      return jsonify(error={'code': 400,
                            'message': 'Bad Request.'}), 400

  @app.errorhandler(401)
  def unauthorized_request(error):
    app.logger.error(error, exc_info=error)
    if type(error.description) == dict:
      return jsonify(error=error.description), 401
    else:
      return jsonify(error={'code': 401,
                            'message': 'Unauthorized Request.'}), 401

  @app.errorhandler(404)
  def page_not_found(error):
    return render_template('tpl/page_404.html'), 404

  @app.errorhandler(405)
  def request_not_found(error):
    app.logger.error(error, exc_info=error)
    if type(error.description) == dict:
      return jsonify(error=error.description), 405
    else:
      return jsonify(error={'code': 405,
                            'message': 'The method is not allowed for the requested URL.'}), 405


app = create_app()
create_api(app)
configure_error_handlers(app)


if __name__ == "__main__":
    app.run(debug=app.config["DEBUG"], port=app.config["PORT"])
