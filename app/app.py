# -*- coding: utf-8 -*-
from flask import Flask
from flask_cors import CORS
from flask_restplus import Api
from werkzeug.exceptions import default_exceptions

# Import API module
from api.account import account_api
from api.login import login_api
from extensions import mongo
import config
from exceptions import api_error_handler


def create_app():
    """Creates the app."""
    # Initialize the app
    app = Flask(__name__)
    app.config.from_object(config)

    CORS(app)
    return app


def create_api(app):
    """ Create the api """
    api = Api(
        title='Api for bank page',
        version='1.0',
        description='A description'
    )
    api.add_namespace(account_api, path='/account_api')
    api.add_namespace(login_api, path='/login_api')
    api.init_app(app)


def configure_extensions(app):
    mongo.init_app(app)

def configure_error_handlers(app):
    """Configures the error handlers."""

    for exception in default_exceptions:
        app.register_error_handler(exception, api_error_handler)
    app.register_error_handler(Exception, api_error_handler)


app = create_app()
create_api(app)
configure_extensions(app)
configure_error_handlers(app)

if __name__ == "__main__":
    app.run(debug=app.config["DEBUG"])
