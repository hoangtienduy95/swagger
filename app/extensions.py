# -*- coding: utf-8 -*-
from flask import Flask
from flask_pymongo import PyMongo
from walrus import Database

from config import MONGO_URI

app = Flask(__name__)
app.config["MONGO_URI"] = MONGO_URI
mongo = PyMongo()

database = Database()
cache_account = database.cache("account")

