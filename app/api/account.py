from flask import jsonify
from flask_restplus import Resource, Namespace
import pymongo
from marshmallow import Schema, fields, validate, ValidationError

from extensions import mongo, cache_account
from config import URL

account_api = Namespace('account_api')


@cache_account.cached(timeout=60)
def get_account_data(page, limit, count, sorted_field, sort_type):
    offset = (page-1)*limit
    query = mongo.db.account.find({}, {"_id": 0}).sort("account_number", pymongo.DESCENDING).skip(offset).limit(limit)
    if sorted_field and sort_type:
        if sort_type == "asc":
            query = mongo.db.account.find({}, {"_id": 0}).sort(sorted_field, pymongo.ASCENDING).skip(offset).limit(limit)
        else:
            query = mongo.db.account.find({}, {"_id": 0}).sort(sorted_field, pymongo.DESCENDING).skip(offset).limit(limit)

    data = {
        "total": count,
        "results": list(query),
        "currentPage": page
    }

    if page == 1:
        data['previous'] = ""
    else:
        data['previous'] = URL + '?page=%d' % (page - 1) + '&limit=%d' % limit

    if page * limit >= count:
        data['next'] = ''
    else:
        data['next'] = URL + '?page=%d' % (page + 1) + '&limit=%d' % limit

    return data


class AccountSchema(Schema):
    balance = fields.Int(validate=validate.Range(min=1))
    firstname = fields.Str(missing="")
    lastname = fields.Str(missing="")
    age = fields.Int(validate=validate.Range(min=1))
    gender = fields.Str()
    address = fields.Str(missing="")
    employer = fields.Str(missing="")
    email = fields.Email(required=True)
    city = fields.Str(missing="")
    state = fields.Str(missing="")


class AccountListSchema(Schema):
    page = fields.Int()
    limit = fields.Int()
    sorted_field = fields.Str()
    sort_type = fields.Str()


parser = account_api.parser()
parser.add_argument('page', location='args')
parser.add_argument('limit', location='args')
parser.add_argument('sorted_field', location='args')
parser.add_argument('sort_type', location='args')
@account_api.route('/account')
class AccountList(Resource):
    @account_api.doc('get_list')
    def get(self):
        page = 1
        limit = 10
        sorted_field = ""
        sort_type = ""
        args = parser.parse_args()

        temp_data = {k: v for k, v in args.items() if v is not None}

        AccountListSchema().load(temp_data)

        if args['page']:
            page = int(args['page'])
        if args['limit']:
            limit = int(args['limit'])
        if args['sorted_field'] is not None:
            sorted_field = args['sorted_field']
        if args['sort_type'] is not None:
            sort_type = args['sort_type']
        count = mongo.db.account.count_documents({})
        data = get_account_data(page, limit, count, sorted_field, sort_type)
        return jsonify(data)

    @account_api.doc('create_account')
    def post(self):
        account_data = AccountSchema().load(account_api.payload)
        email = account_data["email"]
        if mongo.db.account.find({'email': email}).count() > 0:
            return jsonify({"Message": "Email is existed"})

        max_account_number = mongo.db.account.find_one(sort=[("account_number", -1)])
        account_number = max_account_number["account_number"] + 1
        account_data["account_number"] = account_number
        mongo.db.account.insert_one(account_data)
        cache_account.flush()
        return jsonify({"Message": "Create is success"})


@account_api.route('/account/<int:id>')
class Account(Resource):
    @account_api.doc('get_account')
    def get(self, id):
        exist = mongo.db.account.find({'account_number': id}).count() > 0
        if not exist:
            return jsonify({"Message": "Account number not found"})

        query = mongo.db.account.find({"account_number": id}, {"_id": 0})
        return jsonify(list(query))

    @account_api.doc('update_account')
    def put(self, id):
        exist = mongo.db.account.find({'account_number': id}).count() > 0
        if not exist:
            return jsonify({"Message": "Account number not found"})
        account_data = AccountSchema().load(account_api.payload)

        email = account_data["email"]
        if mongo.db.account.find({'email': email}).count() > 0:

            account_number_check = mongo.db.account.find({"email": email}, {"account_number": 1})
            if id == account_number_check[0].get("account_number"):
                account_number = {"account_number": id}
                mongo.db.account.update_one(account_number, {"$set": account_data})
                cache_account.flush()
                return jsonify({"Message": "Account update successfully"})
            return jsonify({"Message": "Email is existed"})

        account_number = {"account_number": id}
        mongo.db.account.update_one(account_number, {"$set": account_data})
        cache_account.flush()
        return jsonify({"Message": "Account update successfully"})

    @account_api.doc('delete_account')
    def delete(self, id):
        exist = mongo.db.account.find({'account_number': id}).count() > 0
        if not exist:
            return jsonify({"Message": "Account number not found"})

        mongo.db.account.delete_one({"account_number": id})
        cache_account.flush()
        return jsonify({"Message": "Account deleted successfully"})