import bcrypt
from marshmallow import Schema, fields, validate, ValidationError
from flask import jsonify
from flask_restplus import Resource, Namespace

from extensions import mongo

login_api = Namespace('login_api')


class LoginSchema(Schema):
    username = fields.Str(required=True, validate=validate.Length(min=1))
    password = fields.Str(required=True)


@login_api.route('/login')
class Login(Resource):
    @login_api.doc('login')
    def post(self):
        login_data = LoginSchema().load(login_api.payload)

        username = login_data["username"]
        if mongo.db.user.find({'username': username}).count() <= 0:
            return jsonify("Invalid username or password")

        password = bytes(login_data["password"], 'utf-8')
        query = mongo.db.user.find({"username": username}, {"password": 1, "_id": 0, "role": 1})
        temp = list(query)
        hashed = temp[0].get('password')
        if bcrypt.checkpw(password, hashed):
            role = temp[0].get('role')
            return jsonify({"Message": "Successful login", "role": role})
        return jsonify("Invalid username or password")
