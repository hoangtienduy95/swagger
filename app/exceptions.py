# -*- coding: utf-8 -*-
from marshmallow import ValidationError
from flask import current_app, jsonify
from werkzeug.exceptions import HTTPException
from http_code import HTTP_STATUS_CODES
from error_code import *


def http_status_message(status_code):
    """Maps an HTTP status code to the textual status"""

    return HTTP_STATUS_CODES.get(status_code, '')


def get_error_message(data):
    try:
        messages = data['messages']
        for k, v in messages.items():
            if isinstance(v, dict):
                for ik, iv in v.items():
                    if isinstance(iv, list) and iv:
                        return iv[0]
                    elif isinstance(iv, str) or isinstance(iv, unicode):
                        return iv
            elif isinstance(v, str) or isinstance(v, unicode):
                return v
            else:
                return 'Validation error, re-check your data.'
    except Exception:
        return 'Validation error, re-check your data.'


def error_data(error_code, message):
    """Constructs a dictionary with status and message for returning in an
    error response"""

    error = {
        'code': error_code,
        'message': message
    }
    return error


class APIException(Exception):
    """The base exception class for all exceptions this library raises."""

    status_code = 500
    http_status_code = HTTP_500_INTERNAL_SERVER_ERROR

    def __init__(self, error_code=None, *args):
        if error_code in self.http_status_code:
            self.error_code = error_code
        else:
            self.error_code = self.status_code
        message = self.http_status_code.get(
            error_code, http_status_message(self.status_code))
        self.message = message.format(*args)

    @property
    def description(self):
        return error_data(self.error_code, self.message)


def api_error_handler(error):

    if isinstance(error, HTTPException):
        code = error.code
        if not isinstance(error.description, dict):
            if code == 422:
                data = getattr(error, 'data')
                error.description = error_data(code, get_error_message(data))
            else:
                error.description = error_data(code, http_status_message(code))
    elif isinstance(error, APIException):
        code = error.status_codisinstancee
    else:
        code = 500
        error.description = error_data(code, http_status_message(code))
    msg = 'HTTP_STATUS_CODE_{0}: {1}'.format(code, error.description)
    if isinstance(error, ValidationError):
        return jsonify(error.messages)
    if code != 404:
        current_app.logger.error(msg, exc_info=error)
    return jsonify(error=error.description), code